"""
MS IO - DataIntensive Pytho nProject
31/01/2021
Main program
"""

import json
import logging
import os
import uuid

import PIL.ExifTags
import PIL.Image
from flask import Flask, Response, request, send_file  # ,redirect
from werkzeug.utils import secure_filename

import helper

# Logger
logging.basicConfig(filename="flaskapi8.log", filemode="w", level=logging.DEBUG)
logging.info("Launching Flask application FLASKAPI 8")

app = Flask(__name__)


# CONFIG VARIABLES :
# Status of thumbnail creation (field "processing_state" in Sqlite Database)
# "init" when image is uploaded, "pending" when thumbnail generation is launched
# and "success" or "failure" if the generation has succeeded or not
app.config["STATUS_INIT"] = "init"
app.config["STATUS_PENDING"] = "pending"
app.config["STATUS_SUCCESS"] = "success"
app.config["STATUS_FAILURE"] = "failure"
# path to store uploaded images
app.config["UPLOAD_FOLDER"] = "uploads/"
# path to thumbnails
app.config["THUMBNAILS_FOLDER"] = "uploads/thumbnails/"
# Only files with following extensions are allowed
app.config["ALLOWED_EXTENSIONS"] = {"png", "jpg", "jpeg", "gif"}
# Maximum size (length AND height) of thumbnails
app.config["THUMBNAIL_max_size"] = 150

# Create (if they dont exist) both directories "./uploads" and "./uploads/thumbnails"
if not os.path.isdir(app.config["THUMBNAILS_FOLDER"]):
    os.makedirs(app.config["THUMBNAILS_FOLDER"])


def allowed_file(filename):
    """Check if the extension of a file is allowed. Test is non-sensitive (pNg is allowed)

    Parameters:
    filename (string): Name of the file

    Returns:
    boolean : True if the filename contains a dot, AND the substring between the LAST
              dot and the end of filename is in config variable app.config["ALLOWED_EXTENSIONS"]

    """
    return (
        "." in filename
        and (filename.rsplit(".", 1)[1]).lower() in app.config["ALLOWED_EXTENSIONS"]
    )


def extension(filename):
    """Get the extension of a filename

    Parameters:
    filename (string): Name of the file

    Returns:
    string : the substring between the LAST dot in the filename, and the end of the filename

    """
    return "." in filename and (filename.rsplit(".", 1)[1]).lower()


def thumbnails(filename):
    """Generate thumbnails

    Parameters:
    filename (string): Name of the image file to reduce

    Returns:
    an image whose both length and height are lower than app.config["THUMBNAIL_max_size"]

    """
    try:
        logging.info(os.path.join(app.config["UPLOAD_FOLDER"], filename))
        logging.info(app.config["THUMBNAILS_FOLDER"] + "tn_" + filename)
        logging.info(os.path.join(app.config["UPLOAD_FOLDER"], filename))
        image = PIL.Image.open(os.path.join(app.config["UPLOAD_FOLDER"], filename))
        # Tricky : we have to use PIL.Image.open because there are two classes "Image"
        # (our own class Image, and one other in PIL)

        max_size = (app.config["THUMBNAIL_max_size"], app.config["THUMBNAIL_max_size"])
        image.thumbnail(max_size)
        # Success saving photo with tn_ prefix in ./upload/thumbnails directory
        image.save(app.config["THUMBNAILS_FOLDER"] + "tn_" + filename)
        image.close()
        return app.config["STATUS_SUCCESS"]
    except IOError:
        # error during thumbnail generation
        return app.config["STATUS_FAILURE"]


@app.route("/")
def hello_world():
    """Route for API homepage

    Parameters:
    filename (string):

    Returns:
    string: a Welcome message with a link to read the API documentation on GITLAB

    """
    return 'MS SIO - FlaskAPI8 - API for image uploading with thumbnail generation - \
           <a href="https://gitlab.com/datazzz/flaskapi8/-/blob/master/README.md">Click here to see \
            documentation (README.md on GITLAB) and learn how to call the API</a>'


@app.route("/images/new", methods=["POST"])
def add_image_file():
    """
    Endpoint for uploading an image file on the server and store its metadata on a database
    Call this api passing a language name and get back its features
    parameters:
      - just an image file (in a POST multipart/form-data)
        in: request
        type: file
        description: The file to upload
    return:
      - image_id : UUID identifier
    responses:
      400:
        description: Bad request if request contains a file part but file is empty
        description: Bad request if request contains a file part but file is empty
      200:
        description: Image uploaded success
    """
    # Get image (data only) from the POST body
    req_data = request.get_json()

    # image = req_data['image']
    image_id = str(
        uuid.uuid4()
    )  # uuid.uuid4() returns UUID('16fd2706-8baf-433b-82eb-8c7fada847da')
    processing_state = app.config["STATUS_INIT"]
    dirname = app.config["UPLOAD_FOLDER"]
    exiftags = ""
    size = 0
    # storage_date = datetime.now()
    logging.info(image_id)

    # First, we extract binary from the request (whose type is enctype="multipart/form-data")
    # and we try to save the file on the physical drive
    if request.method == "POST":
        # check if the post request has the file part
        if "file" not in request.files:
            response = Response(
                "{'error': 'No file part in request - '}",
                status=400,
                mimetype="application/json",
            )
            return response
        file = request.files["file"]
        # if user does not select file, browser also
        # submit a empty part without filename
        if file.filename == "":
            response = Response(
                "{'error': 'Request contains a file part but file is empty - '}",
                status=400,
                mimetype="application/json",
            )
            return response
        if file:
            if not allowed_file(file.filename):
                response = Response(
                    "{'error': 'You can only upload PNG JPG/JPEG or GIF files - '}",
                    status=400,
                    mimetype="application/json",
                )
                return response
            if allowed_file(file.filename):
                filename = secure_filename(file.filename)
                processing_state = app.config["STATUS_PENDING"]
                ext = extension(filename)
                file.save(os.path.join(dirname, filename))
                size = os.path.getsize(os.path.join(dirname, filename))
                # big image is saved, so we can create thumbnail now
                thumbnails(
                    filename
                )  # create thumbnail and store it in sub-directory "uploads/thumbnails"
                # TO DO  launch a celery task to create thumbnail

                # Now that image is stored on drive, record its metadata in the database

                res_data = helper.add_to_list(
                    image_id, processing_state, filename, dirname, exiftags, ext, size
                )
                logging.info(res_data)
                # Return error if image not added
                if res_data is None:
                    response = Response(
                        "{'error': 'Error when recording metadata in database - '}"
                        + image_id,
                        status=400,
                        mimetype="application/json",
                    )
                    return response

                response = Response(
                    '{"message": "Image saved on drive", "image_id":"%s",  "filename":"%s", "extension":"%s", "size":"%s", "thumbnail":"%s"}'
                    % (
                        image_id,
                        os.path.join(app.config["UPLOAD_FOLDER"], filename),
                        ext,
                        size,
                        os.path.join(app.config["THUMBNAILS_FOLDER"], "tn_" + filename),
                    ),
                    status=200,
                    mimetype="application/json",
                )
                return response

    # Return response
    response = Response(json.dumps(res_data), mimetype="application/json")

    return response


@app.route("/images/all")
def get_all_images():
    """Get infos about all images stored in the database

    Parameters:
    (string): None

    Returns:
    integer : count
    list : JSON dictionary with infos

    """
    # Get all images from the helper function
    res_data = helper.get_all_images()
    # Return response
    response = Response(json.dumps(res_data), mimetype="application/json")
    return response


@app.route("/images/all_ids")
def get_all_images_ids():
    """Get identifiers of all images stored in the database

    Parameters:
    (string): None

    Returns:
    integer : count
    list : JSON dictionary with list of identifiers

    """
    # Get all images identifiers from the helper
    res_data = helper.get_all_images_ids()
    # Return response
    response = Response(json.dumps(res_data), mimetype="application/json")
    return response


# get infos about a specific image
@app.route("/images/<image_id>", methods=["GET"])
def get_image():
    """Get all infos about ONE image by its UUID identifier

    Parameters:
    (string): image_id

    Returns:
    200 : JSON with infos about the image
    404 : Error 404 if no image exists with that image_id

    """

    # Return 404 if image_id not found
    if image_id is None:
        response = Response(
            "{'error': 'Cannot find image - please provide a valid image_id '}",
            status=404,
            mimetype="application/json",
        )
        return response

    res_data = helper.get_image(image_id)
    # Return status
    response = Response(json.dumps(res_data), status=200, mimetype="application/json")
    return response


# get processing_state about a specific image
@app.route("/images/processing_state/<image_id>", methods=["GET"])
def get_image_processing_state(image_id):
    """Get the processing_state (i.e the status of thumbnail generation)
    of an  image identified by it's image_id

    Parameters:
    (string): image_id

    Returns:
    200 : processing_state among init/pending/success/failure if image is found
    404 : Error 404 if no image exists with that image_id

    """

    # Get parameter image_id from the URL
    # image_id = request.args.get('image_id')
    # Get processing_state from the helper
    processing_state = helper.get_image_processing_state(image_id)

    # Return 404 if image_id not found
    if processing_state is None:
        response = Response(
            "{'error': 'Image Not Found for image_id '}" + image_id,
            status=404,
            mimetype="application/json",
        )
        return response

    # Return status
    res_data = {"processing_state": processing_state}

    response = Response(json.dumps(res_data), status=200, mimetype="application/json")
    return response


@app.route("/images/update_processing_state", methods=["PUT"])
def update_processing_state():
    """Update the processing_state (i.e the status of thumbnail generation)
        of an  image identified by it's image_id

    Parameters:
    (string): image_id

    Returns:
    200 : processing_state if updating succeed
    400 : Bad request if updating the field fails

    """
    # Get image_id of the image whose processing_update has to be updated from the POST body
    req_data = request.get_json()
    image_id = req_data["image_id"]
    processing_state = req_data["processing_state"]

    # Update image in the list
    res_data = helper.update_processing_state(image_id, processing_state)
    if res_data is None:
        response = Response(
            "{'error': 'Error updating processing_state for image - '"
            + image_id
            + ", "
            + processing_state
            + "}",
            status=400,
            mimetype="application/json",
        )
        return response

    # Return response
    response = Response(json.dumps(res_data), mimetype="application/json")

    return response


@app.route("/images/delete/<image_id>", methods=["DELETE"])
def delete_image(image_id):
    """Delete an image identified by it's image_id

    Parameters:
    (string): image_id

    Returns: the ID of the deleted image
    200 : if deleting succeed
    400 : Bad request if deleting the image in database fails

    """

    # Delete image from the list
    res_data = helper.delete_image(image_id)
    if res_data is None:
        response = Response(
            "{'error': 'Error deleting image - '" + image_id + "}",
            status=400,
            mimetype="application/json",
        )
        return response

    # Return response
    response = Response(json.dumps(res_data), mimetype="application/json")

    return response


# get infos about a specific image
@app.route("/images/thumbnail_details/<string:image_id>", methods=["GET"])
def get_thumbnail_details(image_id):
    """Get infos about a thumbnail of an uploaded image identified by its UUID identifier

    Parameters:
    (string): image_id of the big image

    Returns:
    200 : the URL
    404 : Error 404 if no image exists with that image_id

    """

    if image_id is None:
        response = Response(
            "{'error': 'Cannot find image - please provide a valid image_id '}",
            status=404,
            mimetype="application/json",
        )
        return response
    filename = helper.get_image_filename(image_id)
    thumbnail_url = os.path.join(
        "http://127.0.0.1:5000", app.config["THUMBNAILS_FOLDER"], "tn_" + filename
    )
    thumbnail_path = os.path.join(app.config["THUMBNAILS_FOLDER"], "tn_" + filename)

    # Return status
    # response = Response(json.dumps(thumbnail_url), status=200, mimetype="application/json")

    response = Response(
        '{"thumbnail_url":"%s", "thumbnail_path":"%s","parent_image_id":"%s"}'
        % (thumbnail_url, thumbnail_path, image_id),
        status=200,
        mimetype="application/json",
    )
    return response


# get infos about a specific image
@app.route("/images/image_url/<string:image_id>", methods=["GET"])
def get_image_url(image_id):
    """Get the URL of an uploaded image identified by its UUID identifier

    Parameters:
    (string): image_id of the uploaded image

    Returns:
    200 : the URL
    404 : Error 404 if no image exists with that image_id

    """

    if image_id is None:
        response = Response(
            "{'error': 'Cannot find image - please provide a valid image_id '}",
            status=404,
            mimetype="application/json",
        )
        return response
    filename = helper.get_image_filename(image_id)
    res_data = os.path.join(
        "http://127.0.0.1:5000", app.config["UPLOAD_FOLDER"], filename
    )

    # Return status
    response = Response(json.dumps(res_data), status=200, mimetype="application/json")
    return response


# get infos about a specific image
@app.route("/images/image_details/<string:image_id>", methods=["GET"])
def get_image_details(image_id):
    """Get all details about an uploaded image identified by its UUID identifier

    Parameters:
    (string): image_id of the uploaded image

    Returns:
    200 : the URL
    404 : Error 404 if no image exists with that image_id

    """

    if image_id is None:
        response = Response(
            "{'error': 'Cannot find image - please provide a valid image_id '}",
            status=404,
            mimetype="application/json",
        )
        return response

    res_data = helper.get_image(image_id)
    # Return status
    response = Response(json.dumps(res_data), status=200, mimetype="application/json")
    return response

    filename = helper.get_image_filename(image_id)
    thumbnail_url = os.path.join(
        "http://127.0.0.1:5000", app.config["STATIC_UPLOAD_FOLDER"], "tn_" + filename
    )
    thumbnail_path = os.path.join(app.config["UPLOAD_FOLDER"], "tn_" + filename)

    # Return status
    # response = Response(json.dumps(thumbnail_url), status=200, mimetype="application/json")

    response = Response(
        '{"thumbnail_url":"%s", "thumbnail_path":"%s","parent_image_id":"%s"}'
        % (thumbnail_url, thumbnail_path, image_id),
        status=200,
        mimetype="application/json",
    )
    return response


@app.route("/images/image/<string:image_id>", methods=["GET"])
def get_image_file(image_id):
    """Get uploaded image identified by its UUID identifier

    Parameters:
    (string): image_id

    Returns:
    200 : the thumbnail
    404 : Error 404 if no image exists with that image_id

    """
    if image_id is None:
        response = Response(
            "{'error': 'Cannot find image - please provide a valid image_id '}",
            status=404,
            mimetype="application/json",
        )
        return response
    filename = helper.get_image_filename(image_id)
    return (
        send_file(
            os.path.join(app.config["UPLOAD_FOLDER"], filename),
            mimetype="image/" + extension(filename) + "'",
        ),
        200,
    )


@app.route("/images/thumbnail/<string:image_id>", methods=["GET"])
def get_thumbnail(image_id):
    """Get the thumbnail of ONE image identified by its UUID identifier

    Parameters:
    (string): image_id

    Returns:
    200 : the uploaded image
    404 : Error 404 if no image exists with that image_id

    """

    if image_id is None:
        response = Response(
            "{'error': 'Cannot find image - please provide a valid image_id '}",
            status=404,
            mimetype="application/json",
        )
        return response
    filename = helper.get_image_filename(image_id)

    return (
        send_file(
            os.path.join(app.config["THUMBNAILS_FOLDER"], "tn_" + filename),
            mimetype="image/" + extension(filename) + "'",
        ),
        200,
    )


if __name__ == "__main__":
    app.run(host="127.0.0.1", port=int("5000"), debug=True)
