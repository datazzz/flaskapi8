from main import allowed_file, extension

"import functions to test with pytest"


def test_extension():
    """test the extension function"""
    assert extension("toto.ext") == "ext"
    assert extension("toto..ext") == "ext"
    assert extension("toto..EXT") == "ext"
    assert extension("toto.ext0.EXT") == "ext"
    assert (
        extension("toto.ext0.EXT.") == ""
    )  # if filename ends with a point, extension is an empty string


################################################################################################
####WARNING : this test requires that a file named "dog.jpg" existse in directory "uploads" ####
################################################################################################


def test_allowed_file():
    """test the allowed_file function"""
    assert allowed_file("toto.png") is True
    assert allowed_file("toto.PNG") is True
    assert allowed_file("toto.pNg") is True
    assert allowed_file("toto.jpg") is True
    assert allowed_file("toto.JPG") is True
    assert allowed_file("toto.jPg") is True
    assert allowed_file("toto.jpeg") is True
    assert allowed_file("toto.JPEG") is True
    assert allowed_file("toto.jPEG") is True
    assert allowed_file("toto.gif") is True
    assert allowed_file("toto.GIF") is True
    assert allowed_file("toto.gIf") is True

    # Test filenames containing 2 dots
    assert allowed_file("toto.tata.png") is True
    assert allowed_file("toto.tata.PNG") is True

    # Test filenames containing 2 consecutive dots
    assert allowed_file("toto..png") is True
    assert allowed_file("toto..PNG") is True

    # Test with allowed extension but not at the end of the file
    assert allowed_file("toto..png.") is False
    assert allowed_file("toto..PNG.") is False

    # Test with forbidden extensions
    assert allowed_file("toto.pdf") is False
    assert allowed_file("toto.PDF") is False
    assert allowed_file("toto.exe") is False
    assert allowed_file("toto.EXE") is False
    assert allowed_file("toto.docx") is False
    assert allowed_file("toto.docx") is False
    assert allowed_file("toto.sh") is False
    assert allowed_file("toto.sh") is False

    # Test filenames containing allowed extensions inside, but ending with forbidden extension
    assert allowed_file("toto.png.pdf") is False
    assert allowed_file("toto.PNG.pdf") is False
    assert allowed_file("toto.jpg.pdf") is False
    assert allowed_file("toto.JPG.pdf") is False
    assert allowed_file("toto.jpeg.pdf") is False
    assert allowed_file("toto.JPEG.pdf") is False
    assert allowed_file("toto.gif.pdf") is False
    assert allowed_file("toto.GIF.pdf") is False
