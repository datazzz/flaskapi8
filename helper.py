"""
MS IO - DataIntensive Pytho nProject
31/01/2021
Helper functions
"""

import os
import sqlite3
import sys

import PIL.Image
from PIL.ExifTags import TAGS

DB_PATH = "./database.db"

# processing state
STATUS_INIT = "init"
STATUS_PENDING = "pending"
STATUS_SUCCESS = "success"
STATUS_FAILURE = "failure"


def get_exif(fn):
    #    ret = {}
    #    i = PIL.Image.open(fn)
    #    info = i._getexif()
    #    for tag, value in info.items():
    #        decoded = TAGS.get(tag, tag)
    #        ret[decoded] = value
    #    return ret
    return {}


def add_to_list(image_id, processing_state, filename, dirname, exiftags, ext, size):
    """
    SQL request to insert all metadata of an image in Sqlite database
    Parameters: image_id, processing_state, filename, dirname, exiftags, ext, size
    Returns: all fields
    """
    try:
        conn = sqlite3.connect(DB_PATH)
        curs = conn.cursor()
        curs.execute(
            "insert into images(image_id, processing_state, filename, \
                 dirname, exiftags, extension, size) \
             values(?,?,?,?,?,?,?)",
            (image_id, processing_state, filename, dirname, exiftags, ext, size),
        )
        conn.commit()
        return {
            "image_id": image_id,
            "processing_state": processing_state,
            "filename": filename,
            "dirname": dirname,
        }
    except sqlite3.Error as err:
        print("Error: ", err)
        return None


todo_list = {}


def get_all_images():
    """
    SQL request to get all fields of all images in Sqlite database
    Parameters: None
    Returns: all fields of all images
    """
    try:
        conn = sqlite3.connect(DB_PATH)
        curs = conn.cursor()
        curs.execute("select * from images")
        rows = curs.fetchall()
        return {"count": len(rows), "images": rows}
    except sqlite3.Error as err:
        print("Error: ", err)
        return None


def get_all_images_ids():
    """
    SQL request to get all identifiers of all images in Sqlite database
    Parameters: None
    Returns: all identifiers of all images
    """
    try:
        conn = sqlite3.connect(DB_PATH)
        curs = conn.cursor()
        curs.execute("select image_id from images")
        rows = curs.fetchall()
        return {"count": len(rows), "images": rows}
    except sqlite3.Error as err:
        print("Error: ", err)
        return None


def get_image(image_id):
    """
    SQL request to get all data about an image in Sqlite database
    Parameters: image_id
    Returns: all fields of the image
    """
    try:
        conn = sqlite3.connect(DB_PATH)
        curs = conn.cursor()
        curs.execute(
            "select image_id, processing_state, filename, dirname, \
                exiftags, extension, size from images where image_id='%s'"
            % image_id
        )
        result = curs.fetchone()  # [0]

        # image = PIL.Image.open(os.path.join(result[3], result[2]))
        exif = get_exif(os.path.join(result[3], result[2]))

        # return result
        return {
            "image_id": result[0],
            "processing_state": result[1],
            "filename": result[2],
            "dirname": result[3],
            "exiftags": exif,
            "extension": result[5],
            "size": result[6],
            "URL of the uploaded image": os.path.join(
                "http://127.0.0.1:5000", "static/uploads/", result[2]
            ),
        }
    except sqlite3.Error as err:
        print("Error: ", err)
        return None


def get_image_processing_state(image_id):
    """
    SQL request to Sqlite database : get processing_state of an  image_id
    Parameters: image_id
    Returns: processing_state
    """
    try:
        conn = sqlite3.connect(DB_PATH)
        curs = conn.cursor()
        curs.execute(
            "select processing_state from images where image_id='%s'" % image_id
        )
        processing_state = curs.fetchone()[0]
        return processing_state
    except sqlite3.Error as err:
        print("Error: ", err)
        return None


def update_processing_state(image_id, processing_state):
    """
    SQL request to update processing_state of an image in Sqlite database
    Parameters: image_id
    Returns: processing_state
    """
    # Check if the passed processing_state is a valid value
    if processing_state.lower().strip() == "init":
        processing_state = STATUS_INIT
    elif processing_state.lower().strip() == "pending":
        processing_state = STATUS_PENDING
    elif processing_state.lower().strip() == "success":
        processing_state = STATUS_SUCCESS
    elif processing_state.lower().strip() == "failure":
        processing_state = STATUS_FAILURE
    else:
        return None
    try:
        conn = sqlite3.connect(DB_PATH)
        curs = conn.cursor()
        curs.execute(
            "update images set processing_state=? where image_id=?",
            (processing_state, image_id),
        )
        conn.commit()
        return {image_id: processing_state}
    except sqlite3.Error as err:
        print("Error: ", err)
        return None


def delete_image(image_id):
    """
    SQL request to delete an image in Sqlite database
    Parameters: image_id
    Returns: image_id the identifier of the deleted image
    """
    try:
        conn = sqlite3.connect(DB_PATH)
        curs = conn.cursor()
        curs.execute("delete from images where image_id=?", (image_id,))
        conn.commit()
        return {"image_id": image_id, "message": "image deleted from the database OK"}
    except sqlite3.Error as err:
        print("Error: ", err)
        return None


def get_image_filename(image_id):
    """
    SQL request to Sqlite database : get filename of an  image_id
    Parameters: image_id
    Returns: filename
    """
    try:
        conn = sqlite3.connect(DB_PATH)
        curs = conn.cursor()
        curs.execute("select filename from images where image_id='%s'" % image_id)
        fn = curs.fetchone()[0]
        return fn
    except sqlite3.Error as err:
        print("Error: ", err)
        return None
