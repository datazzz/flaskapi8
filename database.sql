CREATE TABLE `images` (
	`image_id`	TEXT NOT NULL,
	`processing_state`	TEXT,
	`filename`	TEXT,
	`dirname`	TEXT,
	`exiftags`	TEXT,
	`extension`	TEXT,
	`size`	INTEGER,
	PRIMARY KEY(`image_id`)
);

