

# FLASKAPI 8 - A Flask API for image uploading and thumbnails generation
## MS IO project - Python Data Intensive course

A minimal API developed with [Flask](http://flask.pocoo.org/) framework for uploading images on a server

The main purpose of this project is to learn how to implement the essential elements in making an API :

- URL Building

- routes

- Calling API with methods GET, POST, PUT, DELETE

- Error Handling

- Testing vith pytest and test coverage with pytest-cov 

- beautiful code with pylint/flask8 and black and isort

- Interacting with Database (SQLite for storing images metadata)

- TO DO LATER : Authentication with Sessions


## How to Run

- Step 1: `git clone https://gitlab.com/datazzz/flaskapi8.git`

- Step 2: Create a virtual environment `python3 -m venv flaskapi8`

- Step 3: Activate the virtual environment  `cd flaskapi8  &&  source bin/activate`

- Step 4: Install the requirements: `pip3 install -r requirements.txt`

- Step 5: run the app `python3 main.py`

## Specs :

  - Logs are written in ./flaskapi8.log

  - Metadata of pictures are stored in database.db (Sqlite)

  - The SQL script for creating a new database is furnished (database.SQL)

  - Uploaded Images will be stored in directory "uploads/" from the root of the FLask project

  - Thumbnails will be stored in directory "uploads/thumbnails" (with a "tn_" suffix)

  - No need to create the directories (they will be automatically created if they don't exist)

  - Only files with following extensions are allowed : PNG, GIF, JPG/JPEG

  - Extension are non case-sensitive : pGn extension is allowed

  - Maximum size (length AND height) of thumbnails is 150 pixels


## Status Codes

FlaskAPI returns the following status codes in its API:

| Status Code | Description |
| :--- | :--- |
| 200 | `OK` |
| 201 | `CREATED` |
| 204 | `DELETED` |
| 400 | `BAD REQUEST` |
| 404 | `NOT FOUND` |
| 500 | `INTERNAL SERVER ERROR` |


## Database structure

Metadata of uplaoded images are stored on a Sqlite database

| Parameter | Type | Description |
| :--- | :--- | :--- |
| `image_id` | `int` | Unique identifier, at the UUID4 format |
| `processing_state` | `string` | Status of the thumbnail generation : INIT/PENDING/SUCCESS/FAILURE |
| `filename` | `string` | The file of the name |
| `dirname` | `string` | The relative storage path from the root of the Flask Application |
| `exiftags` | `string` | Technical metadata about the picture |
| `extension` | `string` | Uploaded file extension |
| `size` | `int` | Size of the Uploaded file in bytes |

A sample database called database.md is available on this GIT repository but you create your own database with the following SQL script


CREATE TABLE `images` (
	`image_id`	TEXT NOT NULL,
	`processing_state`	TEXT,
	`filename`	TEXT,
	`dirname`	TEXT,
	`exiftags`	TEXT,
	`extension`	TEXT,
	`size`	INTEGER,
	PRIMARY KEY(`image_id`)
);


## Limitations :

  - EXIF data are available only for JPG/JPEG images


## Pylint
```javascript
        ```
        $ pylint main.py
        ************* Module main
        main.py:208:0: C0301: Line too long (287/100) (line-too-long)
        main.py:536:0: C0304: Final newline missing (missing-final-newline)
        main.py:139:4: W0612: Unused variable 'req_data' (unused-variable)
        main.py:273:7: E0602: Undefined variable 'image_id' (undefined-variable)
        main.py:281:32: E0602: Undefined variable 'image_id' (undefined-variable)

        ------------------------------------------------------------------
        Your code has been rated at 9.12/10 (previous run: 9.12/10, +0.00)


        $ pylint helper.py
        ************* Module helper
        helper.py:196:0: C0304: Final newline missing (missing-final-newline)
        helper.py:18:0: R0913: Too many arguments (7/5) (too-many-arguments)

        ------------------------------------------------------------------
        Your code has been rated at 9.79/10 (previous run: 9.88/10, -0.09)


```


## Sample pictures to test the API :

  - 20 sample pictures are available in directory ./testpics

  - For six animals (cat, dog, elephant, fox,  horse, mouse ), there are 4 pictures : one GIF, one JPG, one JPEG, one PNG

  - JPG and JPEG files are identical, but the other pictures (PNG and GIF) are distinct.

  - So there are 6x3=18 different pictures for you to test the uploading system.


## How to call the API (samples)


The CURL commands below are for Linux. If you useCURL on  Windows, you have to escape double  quotes !

  # POST : to upload a new image on the server :

    here are 4 identical CURL comands. 
    
    The only thing that changes is the file extension (CAT, PNG, JPG, JPEG). 
    
    Commands have to be launched from a CLI, from the root of the project, 
    
    because sample images are in directory ./testpics


```javascript
    {
    curl -v -X POST -F file=@'./testpics/dog.jpg' http://127.0.0.1:5000/images/new

    Response :
      {
        "message": "Image saved on drive",
        "image_id": "577abfae-42c3-4358-9902-c414b486c6b2",
        "filename": "uploads/dog.jpg",
        "extension": "jpg",
        "size": "5290",
        "thumbnail": "uploads/thumbnails/tn_dog.jpg"
      }


    curl -v -X POST -F file=@'./testpics/cat.jpeg' http://127.0.0.1:5000/images/new

    Response :
      {
        "message": "Image saved on drive",
        "image_id": "769aef6a-61b4-4db6-96aa-928eccb9c893",
        "filename": "uploads/cat.jpeg",
        "extension": "jpeg",
        "size": "143638",
        "thumbnail": "uploads/thumbnails/tn_cat.jpeg"
      }

    curl -v -X POST -F file=@'./testpics/horse.gif' http://127.0.0.1:5000/images/new

    Response :
      {
        "message": "Image saved on drive",
        "image_id": "b7a26d59-3860-470f-9f8a-ccf17c464259",
        "filename": "uploads/horse.gif",
        "extension": "gif",
        "size": "2237389",
        "thumbnail": "uploads/thumbnails/tn_horse.gif"
      }


    curl -v -X POST -F file=@'./testpics/mouse.png' http://127.0.0.1:5000/images/new

    Response :
      {
        "message": "Image saved on drive",
        "image_id": "a81e0ed5-fd9b-4742-8b1b-e1de87e1fa99",
        "filename": "uploads/mouse.png",
        "extension": "png",
        "size": "280798",
        "thumbnail": "uploads/thumbnails/tn_mouse.png"
      }

    }
```


# GET : Get list of all uploaded images (with a counter)
```javascript
    {
      curl -X GET http://127.0.0.1:5000/images/all
      Response :
      {
      "count": 3,
      "images": [
        [
          "577abfae-42c3-4358-9902-c414b486c6b2",
          "pending",
          "dog.jpg",
          "uploads/",
          "",
          "jpg",
          5290
        ],
        [
          "769aef6a-61b4-4db6-96aa-928eccb9c893",
          "pending",
          "cat.jpeg",
          "uploads/",
          "",
          "jpeg",
          143638
        ],
        [
          "b7a26d59-3860-470f-9f8a-ccf17c464259",
          "pending",
          "horse.gif",
          "uploads/",
          "",
          "gif",
          2237389
        ]
        ]
      }
    }
```

  # GET : Get all details about an uploaded image identified by its ID : its identifier, its URL, its size, its extension, the path where it's stored

      Warning : URL is correct, but by default Flask refuses direct access to files that are not in the "static" directory - Reconfigure your server if you want to display the picture from a web browser... 

```javascript
       curl -v -X GET http://127.0.0.1:5000/images/image_details/577abfae-42c3-4358-9902-c414b486c6b2
      {
      Response :
      {
        "image_id": "577abfae-42c3-4358-9902-c414b486c6b2",
        "processing_state": "pending",
        "filename": "dog.jpg",
        "dirname": "uploads/",
        "exiftags": {},
        "extension": "jpg",
        "size": 5290,
        "URL of the uploaded image": "http://127.0.0.1:5000/static/uploads/dog.jpg"
      }
```



  # GET : Get all details about the thumbnail : its URL, the path where it's stored, and the ID of the big image

      Warning : URL is correct, but by default Flask refuses direct access to files that are not in the "static" directory - Reconfigure your server to do so... 

```javascript
    {
    curl -v -X GET http://127.0.0.1:5000/images/thumbnail_details/577abfae-42c3-4358-9902-c414b486c6b2
    Response :
    {
      "thumbnail_url": "http://127.0.0.1:5000/uploads/thumbnails/tn_dog.jpg",
      "thumbnail_path": "uploads/thumbnails/tn_dog.jpg",
      "parent_image_id": "577abfae-42c3-4358-9902-c414b486c6b2"
    }
    }
```


  # GET : Get the uploaded image (=the file of the big picture, not its metadata !)
       
    Warning : URL is correct, but by default Flask refuses direct access to files that are not in the "static" directory - Reconfigure your server to do so... )


    Quand on essaie de récupérer une image, on obtient un message d'avertissement indiquant qu'il n'est pas conseillé de recevoir le flux directement, et qui conseill eune optin pour forcer l'enregistrement du fichier sur le disque :

```javascript
    {
      curl -v -X GET http://127.0.0.1:5000/images/image/577abfae-42c3-4358-9902-c414b486c6b2
      Response :
          Warning: Binary output can mess up your terminal. Use "--output -" to tell 
          Warning: curl to output it to your terminal anyway, or consider "--output 
          Warning: <FILE>" to save to a file.
          * Failed writing body (0 != 5290)
          * Closing connection 0
    }
```
   
      On relance donc la commande avec l'option --output suivi du nom qu'on souhaite donner au fichier :

```javascript
    {
        curl -v -X GET http://127.0.0.1:5000/images/image/577abfae-42c3-4358-9902-c414b486c6b2 --output ma_grosse_image.extension
      Response :
        {
              < 
          [5290 bytes data]
          100  5290  100  5290    0     0   738k      0 --:--:-- --:--:-- --:--:--  738k
          * Closing connection 0


        }
      }
    }
```



  # GET : Get the thumbnail (=the image file of the tiny picture, not its medadata !)
       
       (warning : URL is correct, but by default Flask refuses direct access to files that are not in the "static" directory - Reconfigure your server to do so... )


```javascript
    {
    curl -v -X GET http://127.0.0.1:5000/images/thumbnail/577abfae-42c3-4358-9902-c414b486c6b2 

    Response :
        {
          Warning: Binary output can mess up your terminal. Use "--output -" to tell 
          Warning: curl to output it to your terminal anyway, or consider "--output 
          Warning: <FILE>" to save to a file.
          * Failed writing body (0 != 3058)
          * Closing connection 0
        }
    }
```

```javascript
    {
    curl -v -X GET http://127.0.0.1:5000/images/thumbnail/577abfae-42c3-4358-9902-c414b486c6b2  --output mon_thumbnail.extension
    Response :
       [3058 bytes data]
        100  3058  100  3058    0     0   373k      0 --:--:-- --:--:-- --:--:--  373k
        * Closing connection 0
    }
```

  # DELETE : Delete an uploaded image in database

```javascript
    { 
    curl -v -X DELETE http://127.0.0.1:5000/images/delete/a81e0ed5-fd9b-4742-8b1b-e1de87e1fa99
          Response :
          {
            "image_id": "a81e0ed5-fd9b-4742-8b1b-e1de87e1fa99",
            "message": "image deleted from the database OK"
          }
    }
```

# GET : Get the field "processing_state" (and only that field) of an image

```javascript
    {
      curl -X GET http://127.0.0.1:5000/images/processing_state/577abfae-42c3-4358-9902-c414b486c6b2
      Response : 
      {
        {"processing_state": "pending"}
      }
   }
```

 # UPDATE : Update the field "processing_state" (and only that field) of the image

```javascript
  {
  curl -X PUT http://127.0.0.1:5000/images/update_processing_state -d '{"image_id": "577abfae-42c3-4358-9902-c414b486c6b2", "processing_state": "pending"}' -H 'Content-Type: application/json'
  }
  Response :
    {
      {"577abfae-42c3-4358-9902-c414b486c6b2": "pending"}
    }
  }
```
